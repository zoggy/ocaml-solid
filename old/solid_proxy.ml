(*********************************************************************************)
(*                OCaml-Solid                                                    *)
(*                                                                               *)
(*    Copyright (C) 2016-2017 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Lesser General Public License version        *)
(*    3 as published by the Free Software Foundation.                            *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public License          *)
(*    along with this program; if not, write to the Free Software                *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** *)

let https_call (profile:Tls.Config.certchain) =
  let certificates = fst profile in
  let authenticator = X509.Authenticator.chain_of_trust
    ~time:(fun() -> None) certificates
  in
  let ctx = Tls.Config.client ~authenticator ~certificates:(`Single profile) () in
  let f ?body ?chunked req =
    Ldp_tls.client_call ~ctx ?body ?chunked
     ~headers: (Cohttp.Request.headers req)
        (Cohttp.Request.meth req)
        (Uri.of_string (Cohttp.Request.resource req))
  in
  Lwt.return f

let http_call ?body ?chunked req =
  Cohttp_lwt_unix.Client.call ?body ?chunked
    ~headers: (Cohttp.Request.headers req)
    (Cohttp.Request.meth req)
    (Uri.of_string (Cohttp.Request.resource req))

type http_call =
  ?body:Cohttp_lwt.Body.t ->
  ?chunked:bool ->
    Cohttp.Request.t -> (Cohttp.Response.t * Cohttp_lwt.Body.t) Lwt.t

let proxy_handler (http_call:http_call) profile =
  fun ?user req body ->
    match Cohttp.Request.meth req with
    | `CONNECT ->
        let r = Cohttp.Response.make () in
        Lwt.return (r, Cohttp_lwt.Body.empty)
    | _ ->
        (*let uri = Uri.of_string (Cohttp.Request.resource req) in*)
        (*let host = match Uri.host uri with
           | None -> failwith (Printf.sprintf "No host in requested resource")
           | Some host -> host
           in
           let port = Uri.port uri in*)
        http_call ~body req

let https_proxy conf profile =
  let%lwt call = https_call profile in
  Server_http_tls.server conf (proxy_handler call profile)
let http_proxy conf profile =
  Server_http.server conf (proxy_handler http_call profile)

