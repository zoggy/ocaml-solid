(*********************************************************************************)
(*                OCaml-LDP                                                      *)
(*                                                                               *)
(*    Copyright (C) 2016-2024 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Lesser General Public License version        *)
(*    3 as published by the Free Software Foundation.                            *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public License          *)
(*    along with this program; if not, write to the Free Software                *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

let main =
  let%lwt h = Ldp_tls.make
    ~cert: ("tutu.pem","tutu.key")
    ~dbg:Lwt_io.(write_line stderr)
    ()
  in
  let module H = (val h : Ldp.Http.Http) in

  let iri = Iri.of_string Sys.argv.(1) in
  let%lwt meta = H.post_container_exn ~slug:"my-directory" iri in
  let%lwt () = Lwt_unix.sleep 1. in
  let%lwt _meta = H.post_container_exn ~slug:"container" meta.Ldp.Types.iri in
  Lwt.return_unit

let () = Lwt_main.run main