Initialize variables:
  $ export URL=https://localhost:9999/
  $ export PATH=${DUNE_SOURCEROOT}/_build/default/tests:${PATH}
  $ export BUILD_PATH_PREFIX_MAP=
Initialize www directory as git repo
  $ rm -fr www
  $ mkdir -p www/repo
  $ cd www/repo
  $ git init . --quiet
  $ mkdir -p dir1/dir2
  $ cp ../../,acl .
  $ /bin/echo -e "file1\nv1" > dir1/file1.txt
  $ /bin/echo -e "file2\nv1" > dir1/dir2/file2.txt
  $ git add ,acl dir1/file1.txt dir1/dir2/file2.txt
  $ git commit -am"v1" --quiet
  $ /bin/echo -e "file1\nv2" > dir1/file1.txt
  $ /bin/echo -e "file2\nv2" > dir1/dir2/file2.txt
  $ git commit -am"v2" --quiet
  $ git log --pretty="%H" | tail -n 1 | cut -d" " -f 1 > ../../commit-id.txt
  $ git show `cat ../../commit-id.txt`:dir1/file1.txt > ../../git1.txt
  $ cat dir1/file1.txt >> ../../git1.txt
  $ git show `cat ../../commit-id.txt`:dir1/dir2/file2.txt > ../../git2.txt
  $ cat dir1/dir2/file2.txt >> ../../git2.txt
  $ cd ../..
Run server, test client:
(use debug log-level to debug)
#for debug only: $ echo ${PWD}
  $ solid-server -c config.json --log-level "app" & sleep 1
  $ export CID=`cat commit-id.txt`
  $ /usr/bin/curl -k -s ${URL}v/${CID}/dir1/file1.txt > file1v1.txt
  $ /usr/bin/curl -k -s ${URL}v/${CID}/dir1/dir2/file2.txt > file2v1.txt
  $ /usr/bin/curl -k -s ${URL}v/master/dir1/file1.txt > file1v2.txt
  $ /usr/bin/curl -k -s ${URL}v/master/dir1/dir2/file2.txt > file2v2.txt
  $ cat file1v1.txt file1v2.txt > all1.txt
  $ cat file2v1.txt file2v2.txt > all2.txt
  $ diff git1.txt all1.txt
  $ diff git2.txt all2.txt
Kill server:
  $ killall solid-server || true
