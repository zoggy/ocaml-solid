(*********************************************************************************)
(*                OCaml-LDP                                                      *)
(*                                                                               *)
(*    Copyright (C) 2016-2024 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Lesser General Public License version        *)
(*    3 as published by the Free Software Foundation.                            *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public License          *)
(*    along with this program; if not, write to the Free Software                *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

let mkdir dir =
  let com = Printf.sprintf "mkdir -p %s" (Filename.quote dir) in
  match%lwt Lwt_process.(exec (shell com)) with
    Unix.WEXITED 0 -> Lwt.return_unit
  | _ -> Lwt.fail_with (Printf.sprintf "Command failed: %s" com)

let iri_base ?basename iri =
  let l =
    match Iri.path iri with
      Iri.Absolute l | Iri.Relative l -> l
  in
  match List.rev l with
    "" :: h :: _ -> h
  | h :: _ -> h
  | _ ->
      match basename with
      | None -> failwith
          (Printf.sprintf "No basename and empty path (%s)" (Iri.to_string iri))
      | Some b -> b

let is_prefix ~s ~pref =
  let len_s = String.length s in
  let len_pref = String.length pref in
  (len_pref <= len_s) &&
    (String.sub s 0 len_pref) = pref

let rec_get http ~dir ?basename iri =
  let module H = (val http : Ldp.Http.Http) in
  let open Ldp.Types in
  let rec iter dir ?(parse=true) iri =
    let%lwt () = Lwt_io.(write_line stderr (Printf.sprintf "get %s" (Iri.to_string iri))) in
    match%lwt H.get ~parse iri with
    | Stdlib.Error e ->
        begin
          let msg = Ldp.Types.string_of_error e in
          match e with
          | Parse_error _
          | Unsupported_format _ ->
              let%lwt () = Lwt_io.(write_line stderr msg) in
              if parse then iter dir ~parse: false iri else Lwt.return_unit
          | _ -> Lwt.return_unit
        end

    | Ok (Container r) ->
        let dir = Filename.concat dir (iri_base ?basename r.meta.iri) in
        let%lwt () = mkdir dir in
        let%lwt children =
          let l = Ldp.Types.container_children r.graph in
          let s_iri = Iri.to_string r.meta.iri in
          let l = List.filter
            (fun iri -> is_prefix ~s:(Iri.to_string iri) ~pref:s_iri)
              l
          in
          Lwt.return l
        in
        Lwt.join
          [
            get_meta dir r ;
            Lwt_list.iter_p (iter dir) children ;
          ]

    | Ok (Rdf r) ->
        let filename = Filename.concat dir (iri_base ?basename r.meta.iri) in
        Lwt_io.(with_file ~mode: Output filename
         (fun oc -> write oc r.contents)
        )
    | Ok (Non_rdf r) ->
        let s = r.contents in
        let filename = Filename.concat dir (iri_base ?basename iri) in
        Lwt_io.(with_file ~mode: Output filename
         (fun oc -> write oc s)
        )
  and iter_opt dir = function
    None -> Lwt.return_unit
  | Some iri -> iter dir iri
  and get_meta dir r =
    Lwt_list.iter_p (iter_opt dir) [ r.meta.acl ; r.meta.meta ]
  in
  iter dir iri

let get_and_print http ~print_type ~raw ?output iri =
  let module H = (val http : Ldp.Http.Http) in
  let%lwt (ct,s) =
    match%lwt H.get_exn ~parse: (not raw) iri with
    | Ldp.Types.Container r
    | Ldp.Types.Rdf r ->
        if raw then
          Lwt.return (r.Ldp.Types.ct, r.contents)
        else
          Lwt.return (Ldp.Ct.ct_turtle, Rdf.Ttl.to_string r.Ldp.Types.graph)
    | Ldp.Types.Non_rdf r -> Lwt.return (r.ct, r.contents)
  in
  match output with
    None ->
      Lwt_io.(
       let%lwt () =
         if print_type then
           write_line stdout (Ldp.Ct.to_string ct) 
         else 
           Lwt.return_unit
       in
       write stdout s)
  | Some fname ->
      Lwt_io.(
       with_file ~mode: Output fname (fun oc -> write oc s)
      )

let print_type = ref false
let raw = ref false
let recursive = ref false
let output = ref None
let basename = ref None
let options =
  [ "--print-type", Arg.Set print_type, " print content-type" ;
    "--raw", Arg.Set raw,
    " print graph source instead of printing from parsed graph" ;

    "-r", Arg.Set recursive,
    " recursively download containers and documents from the given iri";

    "-o", Arg.String (fun s -> output := Some s),
    "name output to given file or directory" ;

    "-b", Arg.String (fun s -> basename := Some s),
    "name basename to use when downloading an iri with an empty path" ;
  ]
let f args http =
  match args with
    [] -> Lwt.return_unit
  | iri :: _ ->
      let iri = Iri.of_string iri in
      if !recursive then
        let dir = match !output with
          | None -> Filename.current_dir_name
          | Some s -> s
        in
        rec_get http ~dir ?basename: !basename iri
      else
        get_and_print http
          ~print_type: !print_type ~raw: !raw ?output: !output iri

let () = Solid_tools.Common.main ~options f
