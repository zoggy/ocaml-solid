(*********************************************************************************)
(*                OCaml-LDP                                                      *)
(*                                                                               *)
(*    Copyright (C) 2016-2024 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Lesser General Public License version        *)
(*    3 as published by the Free Software Foundation.                            *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public License          *)
(*    along with this program; if not, write to the Free Software                *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** Managing cookies. *)

(** Creates a fresh cookie store. *)
module Make : functor () ->
  sig
    (** [clear ()] removes all cookies from the store.*)
    val clear : unit -> unit

    (** [remove_expired_cookies] removes from the store the cookies
      whose expiration date is reached (using {!Unix.time}). *)
    val remove_expired_cookies : unit -> unit

    (** A a cookie for the given IRI and header. *)
    val add_cookie : Iri.t -> Cohttp.Cookie.Set_cookie_hdr.t -> unit

    (** Returns cookies associated to the given IRI. *)
    val cookies_by_iri : Iri.t -> Cohttp.Cookie.cookie list
  end