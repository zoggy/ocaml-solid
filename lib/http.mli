(*********************************************************************************)
(*                OCaml-LDP                                                      *)
(*                                                                               *)
(*    Copyright (C) 2016-2024 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Lesser General Public License version        *)
(*    3 as published by the Free Software Foundation.                            *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public License          *)
(*    along with this program; if not, write to the Free Software                *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** Providing high-level HTTP queries. *)

module Ldp = Rdf.Ldp
open Cohttp

(** String added in query header "User-agent". *)
val user_agent : string ref

type query_error = Cohttp.Code.meth * int * Types.meta

type Types.error +=
  | Query_error of query_error

val string_of_query_error : query_error -> string

(** The set of known container types. *)
val container_types : Iri.Set.t

(** [type_is_container iri] returns whether [iri] is a container type,
  comparing to known {!container_types}. *)
val type_is_container : Iri.t -> bool

(** [is_container ~iri g] returns whether [iri] is a container,
  according to graph [g]. If [iri] is not provided, the name
  if the graph is used intead. *)
val is_container : ?iri: Iri.t -> Rdf.Graph.graph -> bool

(** [response_metadata iri (resp, body)] creates a {!Types.type-meta} structure
  with the given [iri] and from the http response and body. *)
val response_metadata : Iri.t -> (Response.t * Cohttp_lwt.Body.t) -> Types.meta

(** [parse_graph iri content_type body] tries to parse graph in [body]
  according to the [content_type]. If no graph [g] is provided, a new
  one with name [iri] is created. The graph (the enriched graph or the created one)
  or an error is returned.*)
val parse_graph : ?g:Rdf.Graph.graph ->
  Iri.t -> Ct.t -> string -> (Rdf.Graph.graph, Types.error) Stdlib.result

(** {2 Abstraction of HTTP requests} *)

module type Requests =
  sig
    (** Debugging function taking a debug message. *)
    val dbg : string -> unit Lwt.t

    (** [call meth iri] performs a HTTP query to [iri] with the method [meth].
       Optional parameters are:
       {ul
         {- [body] is used to provide the request body, empty by default.}
         {- [headers] is the header of the request, empty by default.}
       }
    *)
    val call: ?body: Cohttp_lwt.Body.t ->
      ?headers: Header.t -> Code.meth -> Iri.t -> (Response.t * Cohttp_lwt.Body.t) Lwt.t
  end

exception Not_initialized of string

(** [dummy_request name] returns a module which can be used in
  applications instead of a normal {!Requests} module, typically
  before some initializations. [dbg] does nothing
  and [call] will raise [Not_initialized name]. *)
val dummy_requests : string -> (module Requests)

(** {2 Caching} *)

module type Cache =
  sig
    (** Clear cache. *)
    val clear : unit -> unit Lwt.t

    (** [get call ?headers iri] looks up in cache for [iri] and [headers]
      and returns the cached response, if any. Else uses [call ?headers iri]
      to retrieve the resource and stores it if request status is ok (code 2XX).
      The [cookie] header is removed from provided headers when looking and
      storing into cache. *)
    val get :
      (?headers: Header.t -> Iri.t ->
       (Response.t * Cohttp_lwt.Body.t) Lwt.t) ->
        ?headers:Header.t -> Iri.t -> (Response.t * string) Lwt.t
  end

(** The different ways the [find] function of a cache implementation
  can respond. *)
type cache_find_response =
  | Not_found (** The resource was not found in cache. *)
  | Found of (Response.t * string) (** The resource was found in cache,
      with the given response and body as string. *)
  | If_error of (Response.t -> string -> (Response.t * string) Lwt.t)
     (** The resource is in the cache, but it should be used only in
         case of error after performing the request. *)

module type Cache_impl =
  sig
    type key
    val clear : unit -> unit Lwt.t

    (** A cache may or may not return a key for the given headers and IRI.
      This allows to create caches for example for some domains only. *)
    val key : Header.t -> Iri.t -> key option

    (** [store key response body] stores [response] and [body] in cache
      for the given [key]. Returns [(response, body)]. *)
    val store : key -> Response.t -> string -> (Response.t * string) Lwt.t

    (** [find key] looks up for key in cache and returns wether if was
      found and how to use it. *)
    val find : key -> cache_find_response Lwt.t
  end

(** Creates a cache from implementation of cache operations. *)
module Make_cache (I:Cache_impl) : Cache

(** A cache caching nothing, i.e. [key] always returns [None]. *)
module No_cache : Cache

(** {2 High-level HTTP} *)

(** The high-level HTTP module created from a {!Requests} implementation.*)

module type Http =
  sig
    val dbg : string -> unit Lwt.t

     (** All [*_exn] functions performing queries may raise a {!Ldp.Types.Error} exception.
      Other query functions may raise such an exception if they only return
      [Ok ...] or [Error query_error]. Functions returning [Ok ...] or
      [Error Types.error] should not raise {!Ldp.Types.Error} exception.

      The optional [headers] parameter is used to specify headers of the query.
      The optional [accept] parameter, when allowed, can be used to specify
      the accepted format of the response body. If specified, it is added
      to the headers of the query.

      The optional [follow] parameter for some functions is used to specify
      how many redirections (i.e. response with 3XX HTTP code) to follow. Default is [1].
    *)

    (** [head iri] performs a HEAD query on [iri] and returns response
      as a {!Ldp.Types.type-meta}. *)
    val head : ?follow:int -> ?headers:Header.t -> Iri.t -> (Types.meta, query_error) result Lwt.t

    val head_exn : ?follow:int -> ?headers:Header.t -> Iri.t -> Types.meta Lwt.t

    (** [get_non_rdf iri] performs a GET query on [iri] and returns
      the content type and body of the response.
      [follow] parameter defaults to 1 and indicate the number of redirections
      to follow (HTTP code 302 or 302).
    *)
    val get_non_rdf : ?follow:int -> ?headers:Header.t ->
      ?accept:Ct.t list -> Iri.t -> ((Ct.t * string), query_error) result Lwt.t

    val get_non_rdf_exn : ?follow:int -> ?headers:Header.t ->
      ?accept:Ct.t list -> Iri.t -> (Ct.t * string) Lwt.t

    (** [get_rdf iri] retrieves a RDF resource at [iri].
      A graph is returned only if optional argument [parse] is [true]
      (default is [false] if no graph is given, else [true]).
      An existing graph can be given with optional argument [g]. If so,
      and if contents must be parsed, this graph is filled by parsing
      the response contents. Else a new graph is created, with [iri] as name.
    *)
    val get_rdf : ?g:Rdf.Graph.graph -> ?follow:int ->
      ?headers:Header.t -> ?accept:Ct.t list -> ?parse: bool -> Iri.t ->
        (string * Rdf.Graph.graph option, Types.error) result Lwt.t

    val get_rdf_exn : ?g:Rdf.Graph.graph -> ?follow: int ->
      ?headers:Header.t -> ?accept:Ct.t list -> ?parse: bool -> Iri.t ->
        (string * Rdf.Graph.graph option) Lwt.t

    (** Same as {!get_rdf} but always parse the response contents to
      return the graph at [iri] (of fill the graph given as [g] argument). *)
    val get_rdf_graph : ?g:Rdf.Graph.graph -> ?follow:int ->
      ?headers:Header.t -> ?accept:Ct.t list -> Iri.t ->
        (Rdf.Graph.graph, Types.error) result Lwt.t

    val get_rdf_graph_exn : ?g:Rdf.Graph.graph -> ?follow:int ->
      ?headers:Header.t -> ?accept:Ct.t list -> Iri.t ->
        Rdf.Graph.graph Lwt.t

    (** Same as {!get_rdf_graph} but may be enhanced in the future. *)
    val get_container : ?g:Rdf.Graph.graph -> ?follow:int ->
      ?headers:Header.t -> ?accept:Ct.t list -> Iri.t ->
        (Rdf.Graph.graph, Types.error) result Lwt.t

    val get_container_exn : ?g:Rdf.Graph.graph -> ?follow:int ->
      ?headers:Header.t -> ?accept:Ct.t list -> Iri.t ->
        Rdf.Graph.graph Lwt.t

    (** [get iri] returns the resource at [iri].
      Optional argument [parse] (default: [true]) indicates whether response contents
      must be parsed or not, when response content-type is [text/turtle] or
      [application/rdf+xml]. If response is not a graph or is not parsed,
      a non-rdf resource is returned. Else if graph is a container, a container
      resource is returned, else a rdf resource is returned.
    *)
    val get : ?follow:int -> ?headers:Header.t -> ?accept:Ct.t list ->
      ?parse:bool -> Iri.t -> (Types.resource, Types.error) result Lwt.t

    val get_exn : ?follow:int -> ?headers:Header.t -> ?accept:Ct.t list ->
      ?parse:bool -> Iri.t -> Types.resource Lwt.t

    (** [post iri] performs a POST query at [iri] and returns the response information.
      Specific optional arguments are:
      {ul
       {- [data] is the body of the query, i.e. the posted data.}
       {- [ct] is the content-type of the posted-data. Default is {!Ldp.Ct.ct_turtle}.}
       {- [slug] is used to suggest the URI of the created resource.}
       {- [typ] indicates the type iri of the created resource.}
      }
    *)
    val post :
      ?headers:Header.t -> ?data:string ->
        ?ct:Ct.t -> ?slug:string -> ?typ:Iri.t -> Iri.t ->
        (Types.meta, query_error) result Lwt.t

    val post_exn :
      ?headers:Header.t -> ?data:string ->
        ?ct:Ct.t -> ?slug:string -> ?typ:Iri.t -> Iri.t ->
        Types.meta Lwt.t

    (** Same as {!post} but specialized to post a new container. The optional
      [data] is a graph, and the type iri of the created resource is set
      to {!Rdf.Ldp.c_BasicContainer}. *)
    val post_container : ?headers:Header.t -> ?data: Rdf.Graph.graph ->
      ?slug:string -> Iri.t -> (Types.meta, query_error) result Lwt.t

    val post_container_exn : ?headers:Header.t -> ?data: Rdf.Graph.graph ->
      ?slug:string -> Iri.t -> Types.meta Lwt.t

    (** Same as {!post_container} but specialized to post a
        {{:https://www.w3.org/TR/2015/REC-ldp-20150226/#dfn-linked-data-platform-direct-container}direct container}.
        The type iri of the created resource is set to {!Rdf.Ldp.c_DirectContainer}. *)
    val post_direct_container : ?headers:Header.t -> ?data: Rdf.Graph.graph ->
      ?slug:string -> ?membershipResource: Iri.t ->
        relation: [< `HasMember of Iri.t | `IsMemberOf of Iri.t ] ->
          Iri.t -> (Types.meta, query_error) result Lwt.t

    val post_direct_container_exn : ?headers:Header.t -> ?data: Rdf.Graph.graph ->
      ?slug:string -> ?membershipResource: Iri.t ->
        relation: [< `HasMember of Iri.t | `IsMemberOf of Iri.t ] ->
          Iri.t -> Types.meta Lwt.t

    (** Same as {!post_container} but specialized to post a
        {{:https://www.w3.org/TR/2015/REC-ldp-20150226/#dfn-linked-data-platform-indirect-container}indirect container}.
        The type iri of the created resource is set to {!Rdf.Ldp.c_IndirectContainer}. *)
    val post_indirect_container : ?headers:Header.t -> ?data: Rdf.Graph.graph ->
      ?slug:string -> ?membershipResource: Iri.t ->
        relation: [< `HasMember of Iri.t | `IsMemberOf of Iri.t ] ->
          insertedContent: Iri.t -> Iri.t -> (Types.meta, query_error) result Lwt.t

    val post_indirect_container_exn : ?headers:Header.t -> ?data: Rdf.Graph.graph ->
      ?slug:string -> ?membershipResource: Iri.t ->
        relation: [< `HasMember of Iri.t | `IsMemberOf of Iri.t ] ->
          insertedContent: Iri.t -> Iri.t -> Types.meta Lwt.t

    (** Same as {!post} but optional data is specified as a rdf graph,
      represented as [text/turtle] in the query.
      Default [typ] is {!Rdf.Ldp.c_RDFSource}. *)
    val post_rdf :
      ?headers:Header.t -> ?data:Rdf.Graph.graph ->
      ?slug:string -> ?typ: Iri.t -> Iri.t -> (Types.meta, query_error) result Lwt.t

    val post_rdf_exn :
      ?headers:Header.t -> ?data:Rdf.Graph.graph ->
      ?slug:string -> ?typ: Iri.t -> Iri.t -> Types.meta Lwt.t

    (** Same as {!post} with optional [typ] set to {! Rdf.Ldp.c_NonRDFSource}.*)
    val post_non_rdf : ?headers:Header.t ->
      ?data:string -> ?ct:Ct.t -> Iri.t -> (Types.meta, query_error) result Lwt.t

    val post_non_rdf_exn : ?headers:Header.t ->
      ?data:string -> ?ct:Ct.t -> Iri.t -> Types.meta Lwt.t

    (** [put iri] performs a PUT query at [iri].
        [data] is the body of the query.
        Default content-type [ct] is [text/turtle].
        Default [typ] is {!Rdf.Ldp.c_nonRdfSource}.*)
    val put : ?headers:Header.t -> ?data:string ->
      ?ct:Ct.t -> ?typ: Iri.t -> Iri.t -> (Types.meta, query_error) result Lwt.t

    val put_exn : ?headers:Header.t -> ?data:string ->
      ?ct:Ct.t -> ?typ: Iri.t -> Iri.t -> Types.meta Lwt.t

    (** Same as {!put} for data is specified as a rdf graph,
      represented as [text/turtle] in the query and
      default [typ] is {!Rdf.Ldp.c_RDFSource}.*)
    val put_rdf : ?headers:Header.t -> data: Rdf.Graph.graph ->
      ?typ:Iri.t -> Iri.t -> (Types.meta, query_error) result Lwt.t

    val put_rdf_exn : ?headers:Header.t -> data: Rdf.Graph.graph ->
      ?typ:Iri.t -> Iri.t -> Types.meta Lwt.t

    (** [patch_with_query iri q] performs a PATCH query at [iri]
      with [q] being a sparql query (represented as string). *)
    val patch_with_query : ?headers:Header.t -> Iri.t -> string ->
      (Types.meta, query_error) result Lwt.t

    val patch_with_query_exn : ?headers:Header.t -> Iri.t -> string -> Types.meta Lwt.t

    (** [patch iri] performs a PATCH query at [iri].
      Optional arguments:
       {ul
        {- [del] specifies triples to delete.}
        {- [ins] specifies triples to add. }
       }
    *)
    val patch : ?headers:Header.t ->
      ?del:Rdf.Graph.graph -> ?ins:Rdf.Graph.graph -> Iri.t ->
        (Types.meta, query_error) result Lwt.t

    val patch_exn : ?headers:Header.t ->
      ?del:Rdf.Graph.graph -> ?ins:Rdf.Graph.graph -> Iri.t -> Types.meta Lwt.t

    (** [delete iri] performs a DELETE query on [iri]. *)
    val delete : ?headers:Header.t -> Iri.t -> (Types.meta, query_error) result Lwt.t

    val delete_exn : ?headers:Header.t -> Iri.t -> Types.meta Lwt.t

    (** [login ?headers iri] performs a HEAD query on [iri] to
      authenticate and returns the user login (the [user] field of the
      {!Ldp.Types.type-meta} structure returned by {!head}. *)
    val login : ?headers:Header.t -> Iri.t ->
      (string option, query_error) result Lwt.t

    val login_exn : ?headers:Header.t -> Iri.t -> string option Lwt.t

    (** [fold_get iris] performs GET queries on each IRI of [iris],
      acting like [fold_left].
        See {!get} for argument [parse]. The optional argument [onerror]
        specified what to do in case of error. Default is to fail ([`Fail]),
        but is is possible to ignore errors ([`Ignore]) or provide a function
        called on the raised exception ([`Report f]).
    *)
    val fold_get :
      ?onerror:[ `Fail | `Ignore | `Report of exn -> unit Lwt.t ] ->
        ?follow:int -> ?headers:Header.t -> ?accept:Ct.t list ->
        ?parse:bool ->
        ('a -> Types.resource -> 'a Lwt.t) ->
        'a -> Iri.t list -> 'a Lwt.t

    (** [fold_get_graph g iris] performs GET queries on each IRI of [iris]
      and merge each retrieved graph into [g]. If a retrieved resource is
      not a RDF resource, it is ignored. See {!fold_get} for optional argument
      [onerror]. *)
    val fold_get_graph :
      ?onerror:[ `Fail | `Ignore | `Report of exn -> unit Lwt.t ] ->
      ?follow:int -> ?headers:Header.t ->
      Rdf.Graph.graph -> Iri.t list -> unit Lwt.t

  end

(** Creates a {!module-type-Http} module using the given cache. *)
module Cached_http : functor (C:Cache) -> Requests -> Http

module Http : Requests -> Http

(** {2 Specialized HTTP queries}

These are queries used to send and receive data of a given content-type. The following
module types and the {!module-Http_ct} module can be used to create functions
to perform such queries.
*)

(** Mapping values of a given content-type to and from strings. *)
module type Ct_wrapper = sig
    type t
    val ct : Ct.t
    val to_string : t -> string
    val of_string : string -> t
  end

(** The type of the module with specialized query functions. *)
module type Http_ct = sig
    type t
    val dbg : string -> unit Lwt.t
    val get : ?follow:int -> ?headers:Header.t  -> Iri.t ->
      (t option * Types.non_rdf_resource, Types.error) result Lwt.t
    val get_exn : ?follow:int -> ?headers:Header.t  -> Iri.t -> (t option * Types.non_rdf_resource) Lwt.t
    val post : ?headers:Header.t -> ?data:t -> ?slug:string ->
      Iri.t -> (t option * Types.meta, query_error) result Lwt.t
    val post_exn : ?headers:Header.t -> ?data:t -> ?slug:string ->
      Iri.t -> (t option * Types.meta) Lwt.t
    val put : ?headers:Header.t -> ?data:t -> Iri.t ->
      (t option * Types.meta, query_error) result Lwt.t
    val put_exn : ?headers:Header.t -> ?data:t -> Iri.t -> (t option * Types.meta) Lwt.t
    val fold_get :
      ?onerror:[ `Fail | `Ignore | `Report of exn -> unit Lwt.t ] ->
        ?follow:int -> ?headers:Header.t ->
        ('a -> t option * Types.non_rdf_resource -> 'a Lwt.t) ->
        'a -> Iri.t list -> 'a Lwt.t
  end

(** Creating a module to perform specialized queries over the given
  {!module-type-Http} module.
  See the corresponding functions in {!module-type-Http} for more information
  about arguments.
*)
module Http_ct (H:Http) (W:Ct_wrapper) : Http_ct with type t = W.t
