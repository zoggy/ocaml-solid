(*********************************************************************************)
(*                OCaml-LDP                                                      *)
(*                                                                               *)
(*    Copyright (C) 2016-2024 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Lesser General Public License version        *)
(*    3 as published by the Free Software Foundation.                            *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public License          *)
(*    along with this program; if not, write to the Free Software                *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** Handling profile information. *)

(** A profile is a RDF graph. *)
type profile = Rdf.Graph.graph

(** A profile defines workspaces, each defines by a title.
  Here we also store IRI of the workspace and all triples
  whose subject if the workspace IRI. *)
type workspace =
  { ws_title : string;
    ws_iri : Iri.t;
    ws_triples : Rdf.Term.triple list;
  }

(** Functions to query a profile. *)
module type S =
  sig
    val get_profile : Iri.t -> (profile, Ldp.Types.error) result Lwt.t
    val webid : profile -> Iri.t
    val inbox : profile -> Iri.t option
    val workspaces : ?typ:Iri.t -> profile -> workspace list
    val storages : profile -> Iri.t list
    val name : profile -> string
    val pim : profile -> Rdf.Pim.from
    val preferences_ws : profile -> workspace list
    val private_ws : profile -> workspace list
    val public_ws : profile -> workspace list
    val shared_ws : profile -> workspace list
    val master_ws : profile -> workspace list
  end

(** Creating functions to query a profile from a HTTP module. *)
module Make : Ldp.Http.Http -> S

